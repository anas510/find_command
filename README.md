This program has functionality like "find" command i.e. it searched recursively in all directories following specified directory for a file with a specific name or file-type.

=> If no starting directory is mentioned then the search will start from PWD.

=> For searching with name give -name like in 'find'.

=> For searching with filetype give argument -type like in 'find'.

=> Also sequence does not matter if you want to use both options

=> Appropriate error handling is also performed here for better and error-free execution.