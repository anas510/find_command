#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

extern int errno;

char* startingDir;			//Dir from where search will begin
char* name=NULL;					//name of file to search if any
char type=0;					//type of file to search for name if any

char findType(long int);

char* findFullPath(char*);
void parseArgs(int,char*[]);
void validateType();
void findFiles();

int main(int argc,char* argv[])
{
	parseArgs(argc,argv);
	//Remove / from end of startingDir if present
	int l=strlen(startingDir);
	if(startingDir[l-1]=='/'  && strcmp(startingDir,argv[1])!=0)
		startingDir[l-1]='\0';
	
	//If the name that user wants to find is our PWD then also display it
	if((name != NULL && type == 'd') || name != NULL)
	{
		char* selfCheck=strstr(startingDir,name);		//Print only if name is same as directory
		if(selfCheck!=NULL && strlen(selfCheck)==strlen(name))		
			printf("%s\n",startingDir);
	}
	else if(type == 'd')
	{
		printf("%s\n",startingDir);
	}
	
	findFiles(startingDir);

	return 0;
}

void findFiles(char* dir)
{
	DIR* dp;
	errno=0;
	dp=opendir(dir);
	
	if(dp==NULL && errno!=0)		//error
	{	
		fprintf(stderr,"Error opening specified directory \"%s\" : ",dir);
		perror("");	
		return;			
	}

	chdir(dir);
	char* fullpath;
	if(strstr(dir,"..")!=NULL)
	{
		fullpath=(char*)malloc(sizeof(char)*PATH_MAX+1);
		realpath(".",fullpath);
	}
	else	
		fullpath=findFullPath(dir);

	/*
		When a recursive call returns then problem begins because CWD is still that directory but now
		we are in parent directory and it searches for directory and files of that directory in child 
		directory where they aren't present so stat function fails and everything starts crashing. Also
		the values of struct stat still retained and when we try to change directory back to parent
		that can't be done because it don't have path for that we have to save Absolute path of parent
		directory so that when child returns we can change CWD to parent's and files can be found.
	*/

	struct dirent * entry;
	errno=0;	
	
	while((entry = readdir(dp))!=NULL)
	{
		struct stat info={};
		if(strcmp(entry->d_name,".")==0 || strcmp(entry->d_name,"..")==0 )
		{			//skip . & .. files
			errno=0;
			continue;
		}

		lstat(entry->d_name, &info);
		if(lstat(entry->d_name, &info)==-1)
		{
			fprintf(stderr,"Error in reading fileType of file %s/%s :  ",fullpath,entry->d_name);
			perror("");
		}
		char fType=findType(info.st_mode);
		if(fType=='\0')
		{
			fprintf(stderr,"Error in type:  ");
			perror("");
		}

		if(fType == 'd')
		{
			long offst=telldir(dp);
			findFiles(entry->d_name);
			
			//Now program will be in some other directory so we have
			// to change it to the one we are currently working in
			
			chdir(fullpath);
			seekdir(dp,offst);
			fType='d';
		}

		if(type!=0 && name!=NULL && type==fType && strcmp(entry->d_name,name)==0)
		{
			//realpath returns path of file pointed to by link but here we need path of link
			if(type=='l')			//Find prints actual path of link not of file it is pointing to
				printf("%s/%s\n",fullpath,entry->d_name);
			else 
				printf("%s\n",realpath(entry->d_name,NULL));
		}
		else if(type==0 || name==NULL)
		{
			if(type=='l' && fType==type)	
				printf("%s/%s\n",fullpath,entry->d_name);
			else if((type!=0 && fType==type) || (name!=NULL && strcmp(entry->d_name,name)==0))
				printf("%s\n",realpath(entry->d_name,NULL));
		}

		errno=0;
	}
	
	if (entry == NULL && errno != 0) 	//Error
	{	
		fprintf(stderr,"Error reading directory %s/%s  : ",fullpath,dir);
		perror("");
	}
	closedir(dp);
	free(fullpath);
	return;
}

void parseArgs(int argc,char* argv[])
{
	if(argc<3){	//display contents of PWD
		printf("Invlaid arguments:\t You should enter atlease 2 parameters for command (-name & fileName to search for)\n");
		exit(0);
	}
	else if(argc==3)
	{
		startingDir = getenv("PWD");
		if(strcmp(argv[1],"-name")==0)
			name=argv[2];
		else if(strcmp(argv[1],"-type")==0)
		{
			type=argv[2][0];
			validateType();
		}
		else
		{
			printf("Invlaid arguments...\n");
			exit(0);
		}
	}
	else if(argc==4)
	{
		startingDir = argv[1];
		if(strcmp(argv[2],"-name")==0)
			name=argv[3];
		else if(strcmp(argv[2],"-type")==0)
		{
			type=argv[3][0];
			validateType();
		}
		else
		{
			printf("Invlaid arguments...\n");
			exit(0);
		}
	}
	else if(argc==5)
	{
		startingDir = getenv("PWD");
		int i;
		if(strcmp(argv[1],"-name")==0)
		{
			i=1;
			name=argv[2];
		}
		else if(strcmp(argv[1],"-type")==0)
		{
			i=2;
			type=argv[2][0];
			validateType();
		}
		else
		{
			printf("Invlaid arguments...\n");
			exit(0);
		}
		
		if(strcmp(argv[3],"-type")==0 && i==1)
		{
			type=argv[4][0];
			validateType();
		}
		else if(strcmp(argv[3],"-name")==0 && i==2)
			name=argv[4];
		else
		{
			printf("Invlaid arguments...\n");
			exit(0);
		}
	}
	else if(argc==6)
	{
		startingDir = argv[1];
		int i;
		if(strcmp(argv[2],"-name")==0)
		{
			i=1;
			name=argv[3];
		}
		else if(strcmp(argv[2],"-type")==0)
		{
			i=2;
			type=argv[3][0];
			validateType();
		}
		else
		{
			printf("Invlaid arguments...\n");
			exit(0);
		}
		
		if(strcmp(argv[4],"-type")==0 && i==1)
		{
			type=argv[5][0];
			validateType();
		}
		else if(strcmp(argv[4],"-name")==0 && i==2)
			name=argv[5];
		else
		{
			printf("You should enter following attributes:\n -type to search for specific file types\n -name to search for specific file by name\n");
			exit(0);
		}
	}
	else
	{
		printf("Invlaid arguments...You should enter 3-6 arguments\n");
	}
}

void validateType()
{
	if(type != 'f' && type != 'l' && type != 'd' && type != 'c' && type != 'b' && type != 'p' && type != 's')
	{
		printf("Invalid type value...Allowed values are f,l,c,b,d,p,s\n");
		exit(0);
	}
}

char findType(long int mode)
{
	int type=mode & 0170000;
	
	//For fileType
	if (type == 0010000)
		return 'p';
	else if (type == 0020000)
		return 'c';
	else if (type == 0040000)
		return 'd';
	else if (type == 0060000)
		return 'b';
	else if (type == 0100000)
		return 'f';
	else if (type == 0120000)
		return 'l';
	else if (type == 0140000)
		return 's';
	return '\0';
}

char* findFullPath(char* dir)
{
	char* path;
	if(strcmp(dir,"/")==0)
		path=dir;
	else if (strstr(dir,"..")!=NULL || strcmp(dir,".")==0)
	{
		path=(char*)malloc(sizeof(char)*PATH_MAX+1);
		realpath(dir,path);
	}
	else
	{
		path=(char*)malloc(sizeof(char)*PATH_MAX+1);				//max path can be 4096 in linux
		realpath(dir,path);
		// fprintf(stderr,"path = %s ,\tdir = %s\n",path,dir);
		if(strcmp(path,startingDir)!=0)			//Remove last entry from path as it is repetitive
		{
			int l1=strlen(dir);
			int l2=strlen(path);
			path[l2-l1-1]='\0';
		}
	}
	// fprintf(stderr,"path = %s ,\tdir = %s\n",path,dir);
	return path;
}